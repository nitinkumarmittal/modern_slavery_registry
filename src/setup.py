"""Setup script."""
from setuptools import setup, find_packages

setup(
    name="modern_slavery_registry",
    version="0.1",
    packages=find_packages(),
)